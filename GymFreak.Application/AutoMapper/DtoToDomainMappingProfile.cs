﻿using AutoMapper;
using GymFreak.Application.DTO;
using GymFreak.Core.Entities.Exercise;

namespace GymFreak.Application.AutoMapper
{
    public class DtoToDomainMappingProfile : Profile
    {
        public DtoToDomainMappingProfile()
        {
            AllowNullDestinationValues = true;
            AllowNullCollections = true;
            CreateMap<ExerciseDto, Exercise>()
                .ForMember(x => x.Specific, opt => opt.Ignore());

            //CreateMap<ExerciseWithSpecificDto, Exercise>()
            //    .ForMember(x => x.Specific.ExerciseBreak, m => m.MapFrom(p => p.ExerciseBreak))
            //    .ForMember(x => x.Specific.Measure, m => m.MapFrom(p => p.Measure))
            //    .ForMember(x => x.Specific.Reps, m => m.MapFrom(p => p.Reps))
            //    .ForMember(x => x.Specific.Sets, m => m.MapFrom(p => p.Sets));
            //.ForMember(x => x.Specific, opt => opt.Ignore());
        }
    }
}