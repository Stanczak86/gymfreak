﻿using AutoMapper;

namespace GymFreak.Application.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static IMapper RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainToDTOMappingProfile());
                cfg.AddProfile(new DtoToDomainMappingProfile());
            }).CreateMapper();
        }
    }
}