﻿using System.Collections.Generic;
using GymFreak.Application.DTO;
using GymFreak.Core.Entities.Exercise;

namespace GymFreak.Application.AutoMapper
{
    public static class ExerciseMapper
    {
        public static ExerciseWithSpecificDto MapToExerciseWithSpecificationDto(this Exercise exercise)
        {
            var model = new ExerciseWithSpecificDto();
            model.Id = exercise.Id;
            model.Name = exercise.Name;
            model.ExerciseBreak = exercise.Specific.ExerciseBreak;
            model.Measure = exercise.Specific.Measure;
            model.Reps = exercise.Specific.Reps;
            model.Sets = exercise.Specific.Sets;

            return model;
        }

        public static IEnumerable<ExerciseWithSpecificDto> MapToExerciseWithSpecificationDto(this IEnumerable<Exercise> exercises)
        {
            foreach (var exercise in exercises)
            {
                var dto = exercise.MapToExerciseWithSpecificationDto();

                yield return dto;
            }
        }
    }
}