﻿using System.Collections.Generic;
using AutoMapper;
using GymFreak.Application.DTO;
using GymFreak.Core.Entities.Exercise;

namespace GymFreak.Application.AutoMapper
{
    public class DomainToDTOMappingProfile: Profile
    {
        public DomainToDTOMappingProfile()
        {
            AllowNullDestinationValues = true;
            AllowNullCollections = true;
            CreateMap<Exercise, ExerciseDto>(MemberList.None)
                .ForSourceMember(x=>x.Specific,opt=>opt.Ignore());
            CreateMap<Exercise, ExerciseWithSpecificDto>()
                .ForPath(s=>s.ExerciseBreak, opt=>opt.MapFrom(src=>src.Specific.ExerciseBreak))
                .ForPath(s=>s.Measure, opt=>opt.MapFrom(src=>src.Specific.Measure))
                .ForPath(s=>s.Reps, opt=>opt.MapFrom(src=>src.Specific.Reps))
                .ForPath(s=>s.Sets, opt=>opt.MapFrom(src=>src.Specific.Sets))
                


            //.ForMember(x => x.ExerciseBreak, m => m.MapFrom(p => p.Specific.ExerciseBreak))
            //.ForMember(x => x.Measure, m => m.MapFrom(p => p.Specific.Measure))
            //.ForMember(x => x.Reps, m => m.MapFrom(p => p.Specific.Reps))
            //.ForMember(x => x.Sets, m => m.MapFrom(p => p.Specific.Sets))

            //.ForSourceMember(x => x.Specific.Exercise, opt => opt.Ignore());
            //.ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null))
            ;


        }
    }
}