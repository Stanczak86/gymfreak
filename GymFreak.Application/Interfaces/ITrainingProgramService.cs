﻿using GymFreak.Core.Entities.Training;

namespace GymFreak.Application.Interfaces
{
    public interface ITrainingProgramService
    {
        void AddTrainingProgramAsync();
        void AddTrainingAsync(int trainingProgramId, Training training);
        void ChangeStatusTrainingProgramAsync(int trainingProgramId,bool isActive );
        void RemoveTrainingFromTrainingProgramAsync(int trainingProgramId,int trainingId);
    }
}