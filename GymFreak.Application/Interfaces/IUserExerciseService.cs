﻿namespace GymFreak.Application.Interfaces
{
    public interface IUserExerciseService
    {
        void AddUserSet(int number, int reps, decimal weight);
    }
}