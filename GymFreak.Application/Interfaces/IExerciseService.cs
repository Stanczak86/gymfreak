﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using GymFreak.Application.DTO;
using GymFreak.Core.Entities.Exercise;

namespace GymFreak.Application.Interfaces
{
    public interface IExerciseService
    {
        Task AddExerciseAsync(string userName, ExerciseWithSpecificDto exercise);
        void AddExerciseSpecificAsync(int exerciseId, ExerciseSpecific exerciseSpecific);
        void UpdateExerciseSpecificForTrainingAsync(int exerciseId);
        Task DeleteExerciseAsync(int exerciseId);
        Task UpdateExerciseAsync(ExerciseWithSpecificDto exercise, string userName);

        Task<IList<ExerciseWithSpecificDto>> GetAllExercisesForUserAsync(string userName);

        Task<ExerciseWithSpecificDto> GetExerciseForUser(int id, string userName);
    }
}