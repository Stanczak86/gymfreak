﻿namespace GymFreak.Application.Interfaces
{
    public interface ITrainingService
    {
        void AddTrainingAsync();
        //void AddExerciseSpecificAsync(int trainingId, TrainingSpecific trainingSpecific);
        void DeleteTrainingAsync(int trainingId);

        void GetAllTrainingsForUser(string userName);
    }
}