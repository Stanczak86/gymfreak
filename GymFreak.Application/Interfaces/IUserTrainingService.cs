﻿using System;
using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Entities.UserTraining;

namespace GymFreak.Application.Interfaces
{
    public interface IUserTrainingService
    {
        void AddUserTrainingToTrainingAsync(int trainingId, UserTraining userTraining);

        void AddUserTrainingAsync(string name, DateTime date, int userId);
        void AddUserExerciseToUserTraining(int userTrainingId, Exercise exercise );
        void DeleteTrainingAsync(int trainingId);
    }
}