﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AutoMapper;
using GymFreak.Application.AutoMapper;
using GymFreak.Application.DTO;
using GymFreak.Application.Interfaces;
using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Interfaces;

namespace GymFreak.Application.Services
{
    public class ExerciseService: IExerciseService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAppLogger<ExerciseService> _logger;
        private readonly IMapper _mapper;

        public ExerciseService(IUnitOfWork unitOfWork, IAppLogger<ExerciseService> logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task  AddExerciseAsync(string userName, ExerciseWithSpecificDto exerciseDto)
        {
            _logger.LogInformation($"Adding new exercise for {userName}");
            var exercise = new Exercise(exerciseDto.Name);
            exercise.SetUpExerciseSpecific(exerciseDto.Reps,exerciseDto.Sets,exerciseDto.ExerciseBreak,exerciseDto.Measure);
            exercise.SetUser(userName);
            await _unitOfWork.ExerciseRepository.AddAsync(exercise);
             _unitOfWork.Commit();
        }

        public void AddExerciseSpecificAsync(int exerciseId, ExerciseSpecific exerciseSpecific)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateExerciseSpecificForTrainingAsync(int exerciseId)
        {
            throw new System.NotImplementedException();
        }

        public async Task DeleteExerciseAsync(int exerciseId)
        {
            _logger.LogInformation($"Delete exercise Id:{exerciseId}");
            var entity = await _unitOfWork.ExerciseRepository.GetByIdAsync(exerciseId);
            if (entity != null)
            {
                _unitOfWork.ExerciseRepository.Delete(entity);
                _unitOfWork.Commit();
            }

            throw new Exception($"No such entity Id:{exerciseId}");
        }

        public async Task UpdateExerciseAsync(ExerciseWithSpecificDto exercise, string userName)
        {
            _logger.LogInformation($"Update exercise Id:{exercise.Id} for User:{userName}");
            var model = await _unitOfWork.ExerciseRepository.GetByIdAsync(exercise.Id);
            model.SetUpExerciseSpecific(exercise.Reps, exercise.Sets,exercise.ExerciseBreak,exercise.Measure);
            model.SetUser(userName);
            model.Updated();
            _unitOfWork.Commit();
        }

        public async Task<IList<ExerciseWithSpecificDto>> GetAllExercisesForUserAsync(string userName)
        {
            _logger.LogInformation($"Get List for user: {userName}");
            var list = await _unitOfWork.ExerciseRepository.ListAsyncWithSpecific(x => x.UserId == userName);
            
            //var listMap = _mapper.Map<List<ExerciseWithSpecificDto>>(list);
            var listMap = list.MapToExerciseWithSpecificationDto().ToList();
            return listMap;
        }

        public async Task<ExerciseWithSpecificDto> GetExerciseForUser(int id, string userName)
        {
            _logger.LogInformation($"Get Exercise for id: {id}");
            var model = await _unitOfWork.ExerciseRepository.GetSingleByExpressionAsyncWithSpecificationInclude(x =>
                x.Id == id && x.UserId == userName);
            var modelMap = model.MapToExerciseWithSpecificationDto();
            //var modelMap = _mapper.Map<ExerciseWithSpecificDto>(model);

            return modelMap;
        }
    }
}