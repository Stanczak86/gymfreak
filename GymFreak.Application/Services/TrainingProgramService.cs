﻿using GymFreak.Application.Interfaces;
using GymFreak.Core.Entities.Training;

namespace GymFreak.Application.Services
{
    public class TrainingProgramService: ITrainingProgramService
    {
        public void AddTrainingProgramAsync()
        {
            throw new System.NotImplementedException();
        }

        public void AddTrainingAsync(int trainingProgramId, Training training)
        {
            throw new System.NotImplementedException();
        }

        public void ChangeStatusTrainingProgramAsync(int trainingProgramId, bool isActive)
        {
            throw new System.NotImplementedException();
        }

        public void RemoveTrainingFromTrainingProgramAsync(int trainingProgramId, int trainingId)
        {
            throw new System.NotImplementedException();
        }
    }
}