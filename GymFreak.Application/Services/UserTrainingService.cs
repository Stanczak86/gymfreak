﻿using System;
using GymFreak.Application.Interfaces;
using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Entities.UserTraining;

namespace GymFreak.Application.Services
{
    public class UserTrainingService: IUserTrainingService
    {
        public void AddUserTrainingToTrainingAsync(int trainingId, UserTraining userTraining)
        {
            throw new NotImplementedException();
        }

        public void AddUserTrainingAsync(string name, DateTime date, int userId)
        {
            throw new NotImplementedException();
        }

        public void AddUserExerciseToUserTraining(int userTrainingId, Exercise exercise)
        {
            throw new NotImplementedException();
        }

        public void DeleteTrainingAsync(int trainingId)
        {
            throw new NotImplementedException();
        }
    }
}