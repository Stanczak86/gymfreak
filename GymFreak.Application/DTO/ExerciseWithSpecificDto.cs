﻿using System;

namespace GymFreak.Application.DTO
{
    public class ExerciseWithSpecificDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Reps { get;  set; }
        public int Sets { get;  set; }
        public TimeSpan ExerciseBreak { get;  set; }
        public string Measure { get;  set; }
    }
}