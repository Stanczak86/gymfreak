﻿namespace GymFreak.Core.Entities.ManyTooManyEntities
{
    public class TrainingExercise
    {
        public int ExerciseId { get; protected set; }
        public Exercise.Exercise Exercise { get; protected set; }
        public int TrainingId { get; protected set; }
        public Training.Training Training { get; protected set; }
    }
}