﻿namespace GymFreak.Core.Entities.ManyTooManyEntities
{
    public class TrainingProgramTraining
    {
        public int TrainingProgramId { get; protected set; }
        public TrainingProgram.TrainingProgram TrainingProgram { get; protected set; }
        public int TrainingId { get; protected set; }
        public Training.Training Training { get; protected set; }
    }
}