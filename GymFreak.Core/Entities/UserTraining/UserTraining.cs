﻿using System;
using System.Collections.Generic;
using System.Linq;
using GymFreak.Core.Entities.UserExercise;
using GymFreak.Core.Enums;
using GymFreak.Core.Interfaces;

namespace GymFreak.Core.Entities.UserTraining
{
    public class UserTraining:BaseEntity,IAggregateRoot
    {
        public string Name { get; protected set; }
        public DateTime Date { get; protected set; }
        public int? TrainingId { get; protected set; }
        public Training.Training Training { get; protected set; }
        private bool saveAsTraining;

        public bool SaveAsTraining
        {
            get { return saveAsTraining; }
            protected set { saveAsTraining = TrainingId!=null?value:false; }
        }


        private readonly List<UserExercise.UserExercise> _exercises = new List<UserExercise.UserExercise>();
        public IReadOnlyCollection<UserExercise.UserExercise> Exercises => _exercises.AsReadOnly();

        private UserTraining()
        {
            
        }
        public UserTraining(string name, DateTime date, int userId)
        {
            Name = name;
            Date = date;
            //AddOneTimeTraining(name, userId);
        }

        public void SaveTrainingAsSchema()
        {
            AddOneTimeTraining(Name,UserId,0);
            this.SaveAsTraining = true;
        }
        public void AddUserExercise( int exerciseId, Exercise.Exercise exercise)
        {
                var set = new UserExercise.UserExercise(this.Id, exercise);
                _exercises.Add(set);
        }


        private void AddOneTimeTraining(string name, string userId, TrainingDaysEnum trainingDay = 0, int? trainingProgramId = null)
        {
            var training = new Training.Training(name, userId, trainingDay = 0, trainingProgramId);
            Training = training;
            TrainingId = training.Id;
        }


    }
}