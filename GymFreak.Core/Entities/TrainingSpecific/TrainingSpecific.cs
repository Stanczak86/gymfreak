﻿using System;
using System.Collections.Generic;
using System.Linq;
using GymFreak.Core.Interfaces;

namespace GymFreak.Core.Entities.TrainingSpecific
{
    public class TrainingSpecific: BaseEntity, IAggregateRoot
    {

        public int TrainingId { get; protected set; }

        private readonly List<Exercise.Exercise> _exercises = new List<Exercise.Exercise>();

        public IReadOnlyCollection<Exercise.Exercise> Exercises => _exercises.AsReadOnly();

        public TrainingSpecific(int trainingId):base()
        {
            TrainingId = trainingId;
        }

        private TrainingSpecific()
        {
            
        }
        public void AddExercise(string name ,int reps,int sets, TimeSpan breakTime, string userId, string measure)
        {
            if (!Exercises.Any(i => i.Name.ToLower() == name.ToLower()))
            {
                var exercise =  new Exercise.Exercise(name, this.Id);
                exercise.SetUpExerciseSpecific(this.Id,reps,sets,breakTime, measure);
                exercise.SetUser(userId);
                _exercises.Add(exercise);
            }
        }

    }
}