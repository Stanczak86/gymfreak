﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GymFreak.Core.Entities.ManyTooManyEntities;
using GymFreak.Core.Enums;
using GymFreak.Core.Interfaces;

namespace GymFreak.Core.Entities.Training
{
    public class Training : BaseEntity, IAggregateRoot
    {
        public string Name { get; protected set; }
        public int? TrainingProgramId { get; protected set; }
        public TrainingDaysEnum TrainingDay { get; protected set; }

        public bool TrainingWithoutProgram => TrainingProgramId == null;
        private readonly List<Exercise.Exercise> _exercises = new List<Exercise.Exercise>();

        public IReadOnlyCollection<Exercise.Exercise> Exercises => _exercises.AsReadOnly();

        public IReadOnlyCollection<TrainingExercise> TrainingExercises { get; protected set; }
        private Training()
        {
            TrainingExercises = new HashSet<TrainingExercise>();
        }
        public Training(string name, string userId, TrainingDaysEnum trainingDay=0, int? trainingProgramId=null):base()
        {
            Name = name;
            TrainingProgramId = trainingProgramId;
            TrainingDay = trainingDay;
            UserId = userId;
        }

        public void AddExercise(string name, int reps, int sets, TimeSpan breakTime, string userId, string measure)
        {
            if (!Exercises.Any(i => i.Name.ToLower() == name.ToLower()))
            {
                var exercise = new Exercise.Exercise(name);
                exercise.SetUpExerciseSpecific( reps, sets, breakTime, measure);
                exercise.SetUser(userId);
                _exercises.Add(exercise);
            }
        }
    }
}