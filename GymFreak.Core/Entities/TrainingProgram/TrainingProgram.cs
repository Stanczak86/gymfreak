﻿using System;
using System.Collections.Generic;
using System.Linq;
using GymFreak.Core.Entities.ManyTooManyEntities;
using GymFreak.Core.Enums;
using GymFreak.Core.Interfaces;

namespace GymFreak.Core.Entities.TrainingProgram
{
    public class TrainingProgram:BaseEntity, IAggregateRoot
    {
        public string Name { get; protected set; }
        public bool IsActual { get; protected set; }

        private readonly List<Training.Training> _trainings = new List<Training.Training>();

        public IReadOnlyCollection<Training.Training> Trainings => _trainings.AsReadOnly();

        public IReadOnlyCollection<TrainingProgramTraining> TrainingProgramTrainings { get; protected set; }
        public TrainingProgram(string name, string userId) : base()
        {
            Name = name;
            UserId = userId;
        }

        private TrainingProgram()
        {
            TrainingProgramTrainings = new HashSet<TrainingProgramTraining>();
        }
        public void AddTraining(string name, string userId, TrainingDaysEnum trainingDay=0)
        {
            if (!Trainings.Any(i => i.Name.ToLower() == name.ToLower() && i.TrainingDay!=0 && i.TrainingDay==trainingDay))
            {
                var training = new Training.Training(name,userId,trainingDay,this.Id);
                _trainings.Add(training);
            }
        }

        public void SetUpTrainingProgramStatus(bool isActual)
        {
            IsActual = isActual;
        }
    }
}