﻿using System.Collections.Generic;
using System.Linq;
using GymFreak.Core.Interfaces;

namespace GymFreak.Core.Entities.UserExercise
{
    public class UserExercise:BaseEntity,IAggregateRoot
    {
        public int UserTrainingId { get; protected set; }
        public string Name { get; protected set; }
        public int? ExerciseId { get; protected set; }

        public Exercise.Exercise Exercise { get; protected set; }

        private readonly List<UserExerciseSet> _sets = new List<UserExerciseSet>();

        public IReadOnlyCollection<UserExerciseSet> Sets => _sets.AsReadOnly();

        private UserExercise()
        {
            
        }
        public UserExercise(int userTrainingId,  Exercise.Exercise exercise):base()
        {
            UserTrainingId = userTrainingId;
            ExerciseId = exercise.Id;
            Exercise = exercise;
        }

        public UserExercise(int userTrainingId) : base()
        {
            UserTrainingId = userTrainingId;
        }

        public void AddUserSet(int number, int reps, decimal weight)
        {
            if (!Sets.Any(i => i.UserExerciseId == this.Id && i.SetNumber== number))
            {
                var set = new UserExerciseSet(number, reps, this.Id, weight);
                _sets.Add(set);
            }
        }
    }
}