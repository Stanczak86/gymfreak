﻿namespace GymFreak.Core.Entities.UserExercise
{
    public class UserExerciseSet:BaseEntity
    {
        public int SetNumber { get; protected set; }
        public int SetReps { get; protected set; }
        public int UserExerciseId { get; protected set; }
        public decimal Weight { get; protected set; }

        private UserExerciseSet()
        {
            
        }
        public UserExerciseSet(int number, int reps, int userExerciseId, decimal weight)
        {
            SetNumber = number;
            SetReps = reps;
            UserExerciseId = userExerciseId;
            Weight = weight;
        }
    }
}