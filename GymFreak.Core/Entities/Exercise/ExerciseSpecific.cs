﻿using System;
using GymFreak.Core.Interfaces;

namespace GymFreak.Core.Entities.Exercise
{
    public class ExerciseSpecific: BaseEntity, IAggregateRoot
    {
      
        public int Reps { get; protected set; }
        public int Sets { get; protected set; }
        public TimeSpan ExerciseBreak { get; protected set; }
        public string Measure { get; protected set; }

        public int ExerciseId { get; protected set; }
        public Exercise Exercise { get; protected set; }
      

        private ExerciseSpecific()
        {
            
        }
        public ExerciseSpecific(int exerciseId, int reps, int sets ,TimeSpan exerciseBreak,string measure=null) :base()
        {
            ExerciseId = exerciseId;
            Reps = reps;
            Sets = sets;
            ExerciseBreak = exerciseBreak;
            Measure = string.IsNullOrEmpty(measure)?"kg": measure;
        }
    }
}