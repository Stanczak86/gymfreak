﻿using System;
using System.Collections;
using GymFreak.Core.Interfaces;

namespace GymFreak.Core.Entities.Exercise
{
    public partial class Exercise: BaseEntity, IAggregateRoot
    {
        public string Name { get; protected set; }
        public ExerciseSpecific Specific { get; protected set; }

        private Exercise()
        {
            
        }
        public Exercise(string name):base()
        {
            Name = name;
        }
        public  void SetUpExerciseSpecific( int reps, int sets, TimeSpan exerciseBreak, string measure=null)
        {
            var specific= new ExerciseSpecific(this.Id,reps,sets, exerciseBreak, measure);
            this.Specific = specific;
        }
    }
}