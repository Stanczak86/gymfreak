﻿using System;

namespace GymFreak.Core.Entities
{
    public  class BaseEntity
    {
      public int Id { get; protected set; }
      public DateTime CreatedAt { get; protected set; }
      public DateTime UpdatedAt { get; protected set; }
      public string UserId { get; protected set; }

        protected BaseEntity()
        {
            CreatedAt=DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }
        public virtual void SetUser(string userId)
        {
            this.UserId = userId;
        }

        public virtual void Updated()
        {
            this.UpdatedAt = DateTime.UtcNow;
        }

    }
}