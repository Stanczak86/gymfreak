﻿using GymFreak.Core.Entities.TrainingProgram;

namespace GymFreak.Core.Interfaces.Repositories
{
    public interface ITrainingProgramRepository: IAsyncRepository<TrainingProgram>
    {
        
    }
}