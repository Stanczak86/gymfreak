﻿using GymFreak.Core.Entities.UserExercise;

namespace GymFreak.Core.Interfaces.Repositories
{
    public interface IUserExerciseRepository:IAsyncRepository<UserExercise>
    {
        
    }
}