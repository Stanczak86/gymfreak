﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GymFreak.Core.Entities.Exercise;

namespace GymFreak.Core.Interfaces.Repositories
{
    public interface IExerciseRepository: IAsyncRepository<Exercise>
    {
        Task<Exercise> GetSingleByExpressionAsyncWithSpecificationInclude(Expression<Func<Exercise, bool>> expression);

        Task<Exercise> GetSingleByExpressionAsyncWithSpecificationExplicitLoad(
            Expression<Func<Exercise, bool>> expression);

        Task<List<Exercise>> ListAsyncWithSpecific(Expression<Func<Exercise, bool>> expression);
    }
}