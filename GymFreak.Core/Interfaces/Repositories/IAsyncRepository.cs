﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GymFreak.Core.Entities;

namespace GymFreak.Core.Interfaces.Repositories
{
    public interface IAsyncRepository<T>:IDisposable where T : BaseEntity
    {
        Task<T> GetByIdAsync(int id);
        Task<List<T>> ListAllAsync();
        Task<List<T>> ListAsync(Expression<Func<T,bool>> expression);
        Task<T> AddAsync(T entity);
        void Update(T entity);
        void Delete(T entity);
        Task<T> GetSingleByExpressionAsync(Expression<Func<T, bool>> expression);
        void Save();

    }
}
