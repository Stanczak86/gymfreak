﻿using GymFreak.Core.Entities.UserTraining;

namespace GymFreak.Core.Interfaces.Repositories
{
    public interface IUserTrainingRepository:IAsyncRepository<UserTraining>
    {
        
    }
}