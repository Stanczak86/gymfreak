﻿using GymFreak.Core.Entities.Training;

namespace GymFreak.Core.Interfaces.Repositories
{
    public interface ITrainigRepository: IAsyncRepository<Training>
    { 

    }
}