﻿using System.Threading.Tasks;

namespace GymFreak.Core.Interfaces
{
    public interface IUserService
    {
        bool CanAccess();
        string GetUserId();
        bool IsAuthenticated();
        Task<string> GetLoggedUserNameEmail(string userName);
    }
}