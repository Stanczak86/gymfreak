﻿using System;
using GymFreak.Core.Interfaces.Repositories;

namespace GymFreak.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IExerciseRepository ExerciseRepository { get; }
        ITrainigRepository TrainingRepository { get;  }
        ITrainingProgramRepository TrainingProgramRepository { get;  }
        IUserExerciseRepository UserExerciseRepository { get;  }
        IUserTrainingRepository UserTrainingRepository { get; }
        bool Commit();

    }
}