﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GymFreak.Infra.Data.Persistance.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "TrainingPrograms",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    IsActual = table.Column<bool>(nullable: false, defaultValue: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingPrograms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trainings",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    TrainingProgramId = table.Column<int>(nullable: true),
                    TrainingDay = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trainings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trainings_TrainingPrograms_TrainingProgramId",
                        column: x => x.TrainingProgramId,
                        principalSchema: "dbo",
                        principalTable: "TrainingPrograms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Exercises",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    TrainingId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exercises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exercises_Trainings_TrainingId",
                        column: x => x.TrainingId,
                        principalSchema: "dbo",
                        principalTable: "Trainings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TrainingProgramTraining",
                schema: "dbo",
                columns: table => new
                {
                    TrainingProgramId = table.Column<int>(nullable: false),
                    TrainingId = table.Column<int>(nullable: false),
                    TrainingId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingProgramTraining", x => new { x.TrainingProgramId, x.TrainingId });
                    table.ForeignKey(
                        name: "FK_TrainingProgramTraining_TrainingPrograms_TrainingId",
                        column: x => x.TrainingId,
                        principalSchema: "dbo",
                        principalTable: "TrainingPrograms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TrainingProgramTraining_Trainings_TrainingId1",
                        column: x => x.TrainingId1,
                        principalSchema: "dbo",
                        principalTable: "Trainings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserTrainings",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    TrainingId = table.Column<int>(nullable: true),
                    SaveAsTraining = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTrainings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserTrainings_Trainings_TrainingId",
                        column: x => x.TrainingId,
                        principalSchema: "dbo",
                        principalTable: "Trainings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseSpecifics",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    Reps = table.Column<int>(nullable: false),
                    Sets = table.Column<int>(nullable: false),
                    ExerciseBreak = table.Column<TimeSpan>(nullable: false),
                    Measure = table.Column<string>(maxLength: 10, nullable: false),
                    ExerciseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseSpecifics", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExerciseSpecifics_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalSchema: "dbo",
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TrainingExercise",
                schema: "dbo",
                columns: table => new
                {
                    ExerciseId = table.Column<int>(nullable: false),
                    ExerciseId1 = table.Column<int>(nullable: true),
                    TrainingId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingExercise", x => new { x.ExerciseId, x.TrainingId });
                    table.ForeignKey(
                        name: "FK_TrainingExercise_Trainings_ExerciseId",
                        column: x => x.ExerciseId,
                        principalSchema: "dbo",
                        principalTable: "Trainings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TrainingExercise_Exercises_ExerciseId1",
                        column: x => x.ExerciseId1,
                        principalSchema: "dbo",
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserExercises",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    UserTrainingId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    ExerciseId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserExercises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserExercises_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalSchema: "dbo",
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserExercises_UserTrainings_UserTrainingId",
                        column: x => x.UserTrainingId,
                        principalSchema: "dbo",
                        principalTable: "UserTrainings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserExerciseSets",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    SetNumber = table.Column<int>(nullable: false),
                    SetReps = table.Column<int>(nullable: false),
                    UserExerciseId = table.Column<int>(nullable: false),
                    Weight = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserExerciseSets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserExerciseSets_UserExercises_UserExerciseId",
                        column: x => x.UserExerciseId,
                        principalSchema: "dbo",
                        principalTable: "UserExercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Exercises_TrainingId",
                schema: "dbo",
                table: "Exercises",
                column: "TrainingId");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseSpecifics_ExerciseId",
                schema: "dbo",
                table: "ExerciseSpecifics",
                column: "ExerciseId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TrainingExercise_ExerciseId1",
                schema: "dbo",
                table: "TrainingExercise",
                column: "ExerciseId1");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingProgramTraining_TrainingId",
                schema: "dbo",
                table: "TrainingProgramTraining",
                column: "TrainingId");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingProgramTraining_TrainingId1",
                schema: "dbo",
                table: "TrainingProgramTraining",
                column: "TrainingId1");

            migrationBuilder.CreateIndex(
                name: "IX_Trainings_TrainingProgramId",
                schema: "dbo",
                table: "Trainings",
                column: "TrainingProgramId");

            migrationBuilder.CreateIndex(
                name: "IX_UserExercises_ExerciseId",
                schema: "dbo",
                table: "UserExercises",
                column: "ExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_UserExercises_UserTrainingId",
                schema: "dbo",
                table: "UserExercises",
                column: "UserTrainingId");

            migrationBuilder.CreateIndex(
                name: "IX_UserExerciseSets_UserExerciseId",
                schema: "dbo",
                table: "UserExerciseSets",
                column: "UserExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTrainings_TrainingId",
                schema: "dbo",
                table: "UserTrainings",
                column: "TrainingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExerciseSpecifics",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "TrainingExercise",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "TrainingProgramTraining",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "UserExerciseSets",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "UserExercises",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Exercises",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "UserTrainings",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Trainings",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "TrainingPrograms",
                schema: "dbo");
        }
    }
}
