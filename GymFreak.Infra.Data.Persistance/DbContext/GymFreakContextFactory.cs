﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using GymFreak.Infra.Data.Persistance.DbContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace GymFreak.Infra.CrossCutting.Identity.DbContext
{

    public class BloggingContextFactory : IDesignTimeDbContextFactory<GymFreakContext>
    {
        public GymFreakContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<GymFreakContext>();
           
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

            return new GymFreakContext(optionsBuilder.Options);
        }
    }
}
