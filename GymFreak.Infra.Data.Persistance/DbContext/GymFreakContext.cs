﻿using System.IO;
using System.Linq;
using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Entities.ManyTooManyEntities;
using GymFreak.Core.Entities.TrainingProgram;
using GymFreak.Core.Entities.UserExercise;
using GymFreak.Core.Entities.UserTraining;
using GymFreak.Infra.Data.Persistance.EntityTypeConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Configuration;
using Training = GymFreak.Core.Entities.Training.Training;

namespace GymFreak.Infra.Data.Persistance.DbContext
{


    public class GymFreakContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public GymFreakContext(DbContextOptions<GymFreakContext> options) : base(options)
        {
        }

        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ExerciseSpecific> ExerciseSpecifies { get; set; }
        public DbSet<Training> Trainings { get; set; }
       // public DbSet<TrainingSpecific> TrainingSpecifies { get; set; }
        public DbSet<TrainingProgram> TrainingPrograms { get; set; }
        public DbSet<UserExercise> UserExercises { get; set; }
        public DbSet<UserExerciseSet> UserExerciseSets { get; set; }
        public DbSet<UserTraining> UserTrainings { get; set; }
        public DbSet<TrainingExercise> TrainingExercise { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("dbo");

            
           builder.Entity<Exercise>(ExerciseConfig.Configure);
           builder.Entity<ExerciseSpecific>(ExerciseSpecificConfig.Configure);
           builder.Entity<Training>(TrainingConfig.Configure);
           builder.Entity<TrainingProgram>(TrainingProgramConfig.Configure);
           builder.Entity<UserExercise>(UserExerciseConfig.Configure);
           builder.Entity<UserExerciseSet>(UserExerciseSetConfig.Configure);
           builder.Entity<UserTraining>(UserTrainingConfig.Configure);
           builder.Entity<TrainingExercise>(TrainingExerciseConfig.Configure);
           builder.Entity<TrainingProgramTraining>(TrainingProgramTrainingConfig.Configure);


            foreach (var relathionship in builder.Model.GetEntityTypes().SelectMany(e=>e.GetForeignKeys()))
            {
                relathionship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // get the configuration from the app settings
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            // define the database to use
            optionsBuilder?.UseSqlServer(config.GetConnectionString("DefaultConnection"));
        }
    }
}