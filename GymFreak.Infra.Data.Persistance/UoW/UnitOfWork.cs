﻿using System;
using GymFreak.Core.Interfaces;
using GymFreak.Core.Interfaces.Repositories;
using GymFreak.Infra.Data.Persistance.DbContext;

namespace GymFreak.Infra.Data.Persistance.UoW
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly GymFreakContext _context;

        public UnitOfWork(GymFreakContext context, IExerciseRepository exerciseRepository, ITrainigRepository trainingRepository, ITrainingProgramRepository trainingProgramRepository, IUserExerciseRepository userExerciseRepository, IUserTrainingRepository userTrainingRepository)
        {
            _context = context;
            ExerciseRepository = exerciseRepository;
            TrainingRepository = trainingRepository;
            TrainingProgramRepository = trainingProgramRepository;
            UserExerciseRepository = userExerciseRepository;
            UserTrainingRepository = userTrainingRepository;
        }

        public IExerciseRepository ExerciseRepository { get; }
        public ITrainigRepository TrainingRepository { get; }
        public ITrainingProgramRepository TrainingProgramRepository { get; }
        public IUserExerciseRepository UserExerciseRepository { get; }
        public IUserTrainingRepository UserTrainingRepository { get; }

        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}