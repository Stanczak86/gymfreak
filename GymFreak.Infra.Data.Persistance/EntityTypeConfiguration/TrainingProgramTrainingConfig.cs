﻿using GymFreak.Core.Entities.ManyTooManyEntities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GymFreak.Infra.Data.Persistance.EntityTypeConfiguration
{
    public class TrainingProgramTrainingConfig
    {
        public static void Configure(EntityTypeBuilder<TrainingProgramTraining> builder)
        {
            builder
                .HasKey(bc => new { bc.TrainingProgramId, bc.TrainingId });

            builder
                .HasOne(bc => bc.TrainingProgram)
                .WithMany(b => b.TrainingProgramTrainings)
                .HasForeignKey(bc => bc.TrainingId);

            //builder
            //    .HasOne(bc => bc.Category)
            //    .WithMany(c => c.BookCategories)
            //    .HasForeignKey(bc => bc.CategoryId);
        }
    }
}
