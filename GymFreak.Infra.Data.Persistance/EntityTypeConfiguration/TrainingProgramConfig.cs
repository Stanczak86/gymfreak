﻿using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Entities.TrainingProgram;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GymFreak.Infra.Data.Persistance.EntityTypeConfiguration
{
    public static class TrainingProgramConfig
    {
        public static void Configure(EntityTypeBuilder<TrainingProgram> builder)
        {
            builder.ToTable("TrainingPrograms");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(255).IsUnicode().IsRequired();
            builder.Property(x => x.IsActual).HasDefaultValue(1);
        }
    }
}