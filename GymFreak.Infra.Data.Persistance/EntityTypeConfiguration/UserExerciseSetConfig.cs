﻿using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Entities.UserExercise;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GymFreak.Infra.Data.Persistance.EntityTypeConfiguration
{
    public static class UserExerciseSetConfig
    {
        public static void Configure(EntityTypeBuilder<UserExerciseSet> builder)
        {
            builder.ToTable("UserExerciseSets");
            builder.HasKey(x => x.Id);
        }
    }
}