﻿using GymFreak.Core.Entities.Exercise;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GymFreak.Infra.Data.Persistance.EntityTypeConfiguration
{
    public static class ExerciseSpecificConfig
    {
        public static void Configure(EntityTypeBuilder<ExerciseSpecific> builder)
        {
            builder.ToTable("ExerciseSpecifics");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Measure).HasMaxLength(10).IsUnicode().IsRequired();
        }
    }
}