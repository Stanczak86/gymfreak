﻿using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Entities.UserExercise;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GymFreak.Infra.Data.Persistance.EntityTypeConfiguration
{
    public static class UserExerciseConfig
    {
        public static void Configure(EntityTypeBuilder<UserExercise> builder)
        {
            builder.ToTable("UserExercises");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(255).IsUnicode().IsRequired();
            builder.HasMany(c => c.Sets).WithOne().IsRequired().OnDelete(DeleteBehavior.Cascade);
        }
    }
}