﻿using GymFreak.Core.Entities.Exercise;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GymFreak.Infra.Data.Persistance.EntityTypeConfiguration
{
    public static class ExerciseConfig
    {
        public static void Configure(EntityTypeBuilder<Exercise> builder)
        {
            builder.ToTable("Exercises");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(255).IsUnicode().IsRequired();
            builder.HasOne(a =>a.Specific).WithOne(i=>i.Exercise).HasForeignKey<ExerciseSpecific>(x => x.ExerciseId);
   
        }
    }
}