﻿using GymFreak.Core.Entities.ManyTooManyEntities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GymFreak.Infra.Data.Persistance.EntityTypeConfiguration
{
    public class TrainingExerciseConfig
    {
        public static void Configure(EntityTypeBuilder<TrainingExercise> builder)
        {
            builder
                .HasKey(bc => new { bc.ExerciseId, bc.TrainingId });

            builder
                .HasOne(bc => bc.Training)
                .WithMany(b => b.TrainingExercises)
                .HasForeignKey(bc => bc.ExerciseId);

            //builder
            //    .HasOne(bc => bc.Category)
            //    .WithMany(c => c.BookCategories)
            //    .HasForeignKey(bc => bc.CategoryId);
        }
    }
}
