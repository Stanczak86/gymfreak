﻿using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Entities.UserTraining;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GymFreak.Infra.Data.Persistance.EntityTypeConfiguration
{
    public static class UserTrainingConfig
    {
        public static void Configure(EntityTypeBuilder<UserTraining> builder)
        {
            builder.ToTable("UserTrainings");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(255).IsUnicode().IsRequired();
            builder.HasMany(c => c.Exercises).WithOne().IsRequired().OnDelete(DeleteBehavior.Cascade);
        }
    }
}