﻿using GymFreak.Core.Entities.UserExercise;
using GymFreak.Core.Interfaces.Repositories;
using GymFreak.Infra.Data.Persistance.DbContext;

namespace GymFreak.Infra.Data.Persistance.Repository
{
    public class UserExerciseRepository: EfRepository<UserExercise>, IUserExerciseRepository
    {
        public UserExerciseRepository(GymFreakContext dbContext) : base(dbContext)
        {
        }
    }
}