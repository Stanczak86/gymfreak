﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Interfaces.Repositories;
using GymFreak.Infra.Data.Persistance.DbContext;
using Microsoft.EntityFrameworkCore;

namespace GymFreak.Infra.Data.Persistance.Repository
{
    public class ExerciseRepository: EfRepository<Exercise>, IExerciseRepository
    {
        public ExerciseRepository(GymFreakContext dbContext) : base(dbContext)
        {
            
        }

        public virtual async Task<Exercise> GetSingleByExpressionAsyncWithSpecificationInclude(Expression<Func<Exercise, bool>> expression)
        {
            var query = _dbContext.Exercises.Where(expression);
            query = query.Include(x=>x.Specific);

            return await query.SingleOrDefaultAsync();
        }

        public virtual async Task<Exercise> GetSingleByExpressionAsyncWithSpecificationExplicitLoad(Expression<Func<Exercise, bool>> expression)
        {
            var exercise = await _dbContext.Exercises.Where(expression).SingleOrDefaultAsync();
            await _dbContext.Entry(exercise).Reference(x => x.Specific).LoadAsync();

            return exercise;
        }

        public async Task<List<Exercise>> ListAsyncWithSpecific(Expression<Func<Exercise, bool>> expression)
        {
            var query = _dbContext.Exercises.AsQueryable().Where(expression);
            query = query.Include(x => x.Specific);

            return await query.ToListAsync();
        }
    }
}