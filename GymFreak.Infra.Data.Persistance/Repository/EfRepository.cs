﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GymFreak.Core.Entities;
using GymFreak.Core.Interfaces.Repositories;
using GymFreak.Infra.Data.Persistance.DbContext;
using Microsoft.EntityFrameworkCore;

namespace GymFreak.Infra.Data.Persistance.Repository
{
    public class EfRepository<T> :  IAsyncRepository<T> where T : BaseEntity
    {
        protected readonly GymFreakContext _dbContext;

        protected EfRepository(GymFreakContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<T> GetSingleByExpressionAsync(Expression<Func<T,bool>> expression)
        {
            var query = _dbContext.Set<T>().Where(expression);

            return await query.SingleOrDefaultAsync(expression);
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public async Task<List<T>> ListAllAsync()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<List<T>> ListAsync(Expression<Func<T, bool>> expression)
        {
            var query = _dbContext.Set<T>().AsQueryable().Where(expression);

            return  await query.ToListAsync();
        }

        public async Task<T>  AddAsync(T entity)
        {
            await _dbContext.Set<T>().AddAsync(entity);
          //  await _dbContext.SaveChangesAsync();

            return  entity;
        }

        public void Update(T entity)
        {
           _dbContext.Entry(entity).State = EntityState.Modified;
          
            // await _dbContext.SaveChangesAsync();
        }

        public void Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
           // await _dbContext.SaveChangesAsync();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}