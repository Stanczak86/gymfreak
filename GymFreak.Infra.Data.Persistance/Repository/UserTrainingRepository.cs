﻿using GymFreak.Core.Entities.UserTraining;
using GymFreak.Core.Interfaces.Repositories;
using GymFreak.Infra.Data.Persistance.DbContext;

namespace GymFreak.Infra.Data.Persistance.Repository
{
    public class UserTrainingRepository: EfRepository<UserTraining>, IUserTrainingRepository
    {
        public UserTrainingRepository(GymFreakContext dbContext) : base(dbContext)
        {
        }
    }
}