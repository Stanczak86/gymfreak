﻿using GymFreak.Core.Entities.TrainingProgram;
using GymFreak.Core.Interfaces.Repositories;
using GymFreak.Infra.Data.Persistance.DbContext;

namespace GymFreak.Infra.Data.Persistance.Repository
{
    public class TrainingProgramRepository: EfRepository<TrainingProgram>, ITrainingProgramRepository
    {
        public TrainingProgramRepository(GymFreakContext dbContext) : base(dbContext)
        {
        }
    }
}