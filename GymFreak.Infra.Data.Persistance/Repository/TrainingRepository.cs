﻿using GymFreak.Core.Entities.Training;
using GymFreak.Core.Interfaces.Repositories;
using GymFreak.Infra.Data.Persistance.DbContext;

namespace GymFreak.Infra.Data.Persistance.Repository
{
    public class TrainingRepository: EfRepository<Training>,ITrainigRepository
    {
        public TrainingRepository(GymFreakContext dbContext) : base(dbContext)
        {
        }
    }
}