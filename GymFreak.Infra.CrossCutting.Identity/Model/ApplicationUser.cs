﻿using Microsoft.AspNetCore.Identity;

namespace GymFreak.Infra.CrossCutting.Identity.Model
{
    public class ApplicationUser : IdentityUser
    {
    }
}