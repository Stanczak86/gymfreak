﻿using System.Security.Claims;
using System.Threading.Tasks;
using GymFreak.Core.Interfaces;
using GymFreak.Infra.CrossCutting.Identity.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace GymFreak.Infra.CrossCutting.Identity.Adapter
{
    public class UserServiceAspIdentityAdapter: IUserAdapter
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserServiceAspIdentityAdapter(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }


        public bool CanAccess()
        {
            throw new System.NotImplementedException();
        }

        public string GetUserId()
        {
            throw new System.NotImplementedException();
        }

        public bool IsAuthenticated()
        {
            throw new System.NotImplementedException();
        }

        public async Task<string> GetLoggedUserNameEmail(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);

            return user?.Email;
        }
    }
}