﻿using System.Text;
using AutoMapper;
using GymFreak.Application.AutoMapper;
using GymFreak.Application.Interfaces;
using GymFreak.Application.Services;
using GymFreak.Core.Entities.Exercise;
using GymFreak.Core.Interfaces;
using GymFreak.Core.Interfaces.Repositories;
using GymFreak.Infra.CrossCutting.Identity.Adapter;
using GymFreak.Infra.CrossCutting.Identity.DbContext;
using GymFreak.Infra.CrossCutting.Logging;
using GymFreak.Infra.Data.Persistance.DbContext;
using GymFreak.Infra.Data.Persistance.Repository;
using GymFreak.Infra.Data.Persistance.UoW;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;


namespace GymFreak.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            
            services.AddDbContext<GymFreakContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection")));


            //// ASP.NET HttpContext dependency
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //// Domain Bus (Mediator)
            //services.AddScoped<IMediatorHandler, InMemoryBus>();

            //// ASP.NET Authorization Polices
            //services.AddSingleton<IAuthorizationHandler, ClaimsRequirementHandler>();

            //// Application
            //services.AddScoped<ICustomerAppService, CustomerAppService>();

            //// Domain - Events
            //services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
            //services.AddScoped<INotificationHandler<CustomerRegisteredEvent>, CustomerEventHandler>();
            //services.AddScoped<INotificationHandler<CustomerUpdatedEvent>, CustomerEventHandler>();
            //services.AddScoped<INotificationHandler<CustomerRemovedEvent>, CustomerEventHandler>();

            //// Domain - Commands
            //services.AddScoped<IRequestHandler<RegisterNewCustomerCommand>, CustomerCommandHandler>();
            //services.AddScoped<IRequestHandler<UpdateCustomerCommand>, CustomerCommandHandler>();
            //services.AddScoped<IRequestHandler<RemoveCustomerCommand>, CustomerCommandHandler>();

            //// Infra - Data
            
            services.AddScoped<IExerciseRepository, ExerciseRepository>();
            services.AddScoped<ITrainigRepository,TrainingRepository >();
            services.AddScoped<ITrainingProgramRepository, TrainingProgramRepository>();
            services.AddScoped<IUserExerciseRepository, UserExerciseRepository>();
            services.AddScoped<IUserTrainingRepository, UserTrainingRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<GymFreakContext>();
            services.AddScoped<AppIdentityDbContext>();

            //Application
            services.AddScoped<IExerciseService, ExerciseService>();
            services.AddSingleton<IMapper>(AutoMapperConfig.RegisterMappings());

            //Logging
            services.AddScoped(typeof(IAppLogger<>), typeof(LoggerAdapter<>));

            //Identity
            services.AddScoped<IUserService, UserServiceAspIdentityAdapter>();
            services.AddScoped<IUserAdapter, UserServiceAspIdentityAdapter>();

            //// Infra - Data EventSourcing
            //services.AddScoped<IEventStoreRepository, EventStoreSQLRepository>();
            //services.AddScoped<IEventStore, SqlEventStore>();
            //services.AddScoped<EventStoreSQLContext>();

            //// Infra - Identity Services
            //services.AddTransient<IEmailSender, AuthEmailMessageSender>();
            //services.AddTransient<ISmsSender, AuthSMSMessageSender>();

            //// Infra - Identity
            //services.AddScoped<IUser, AspNetUser>();

            //Website

        }

        public static void RegisterLoggerConfigure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            env.ConfigureNLog("nlog.config");

            //add NLog to ASP.NET Core
            loggerFactory.AddNLog();

            //add NLog.Web
            app.AddNLogWeb();
        }
    }
}