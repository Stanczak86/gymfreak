﻿using System.Collections.Generic;
using GymFreak.Website.ViewModels.Training;
using GymFreak.Website.ViewModels.TrainingProgram;

namespace GymFreak.Website.Interfaces
{
    public interface ITrainingProgramService
    {
        void CreateTrainingProgramForUser(string user);
        void AddTrainingToTrainingProgram(int trainingId);
        void RemoveTrainingProgramF(int exerciseId);
        List<SchemaViewModel> GetTrainingsForProgram(int programId);
        List<TrainingProgramViewModel> GetAllTrainingProgramsForUser(string user);
    }
}