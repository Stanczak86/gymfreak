﻿namespace GymFreak.Website.Interfaces
{
    public interface IUserService
    {
        string GetLoggedUserName();
    }
}