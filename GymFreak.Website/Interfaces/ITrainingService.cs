﻿using System.Collections.Generic;
using GymFreak.Website.ViewModels.Exercise;
using GymFreak.Website.ViewModels.Training;

namespace GymFreak.Website.Interfaces
{
    public interface ITrainingService
    {
        void CreateTrainingForUser(string user);
        void EditTraining(int trainingId);
        void RemoveExerciseFromTraining(int exerciseId);
        void AddExerciseToTraining(int exerciseId);
        void MakeNewProgramWithThisTraining();
        List<ExerciseViewModel> GetExerciseForTraining(int trainingId);
        List<SchemaViewModel> GetAllTrainingsForUser(string user);
    }


}