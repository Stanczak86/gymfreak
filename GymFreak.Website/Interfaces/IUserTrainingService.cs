﻿using System.Collections.Generic;
using GymFreak.Website.ViewModels.UserTraining;

namespace GymFreak.Website.Interfaces
{
    public interface IUserTrainingService
    {
        void CreateUserTrainingForUser(string user, NewUserTrainingViewModel trainingModel);
        void SetUpUserTrainingAsTraining(int userTrainingId);
        void AddUserExerciseToUserTraining();
        void AddSetForUserExerciseInTraining(UserTrainingSetViewModel set, int userExerciseId);
        List<UserTrainingViewModel> GetAllUserTrainings(string user);
    }
}