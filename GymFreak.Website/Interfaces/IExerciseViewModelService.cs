﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GymFreak.Website.ViewModels.Exercise;

namespace GymFreak.Website.Interfaces
{
    public interface IExerciseViewModelService
    {
        Task CreateExerciseForUser(string user, ExerciseViewModel exerciseViewModel);
        Task UpdateExercise(ExerciseViewModel exerciseModel, string userName);
        void RemoveExercise(int exerciseId);
        Task<List<ExerciseViewModel>> GetAllExercisesForUser(string user);
        Task<ExerciseViewModel> GetExercise(int id, string userName);
        Task DeleteExercise(int id, string userName);
    }
}