﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymFreak.Application.Interfaces;
using GymFreak.Infra.CrossCutting.Identity.Adapter;
using GymFreak.Website.Interfaces;
using GymFreak.Website.ViewModels.Exercise;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GymFreak.Website.Controllers
{
    [Authorize]
    public class ExerciseController : Controller
    {
        private readonly IExerciseViewModelService _exerciseService;
        private readonly IUserAdapter _userService;

        public ExerciseController(IExerciseViewModelService exerciseService, IUserAdapter userService)
        {
            _exerciseService = exerciseService;
            _userService = userService;
        }

        // GET: Exercise
        public async Task<ActionResult> Index()
        {
            var name = User.Identity.Name;
            var userName = await _userService.GetLoggedUserNameEmail(name);
            if (userName == null)
            {
                return NotFound();
            }

            var model = await _exerciseService.GetAllExercisesForUser(userName);

            return View(model);
        }

        // GET: Exercise/Create
        public ActionResult Create()
        {
            ExerciseViewModel model = new ExerciseViewModel();

            return View(model);
        }

        // POST: Exercise/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create(IFormCollection collection)
        public async Task<ActionResult> Create( ExerciseViewModel exercise)
        {
            try
            {
                // TODO: Add insert logic here
                var name = User.Identity.Name;
                var userName = await _userService.GetLoggedUserNameEmail(name);
                await _exerciseService.CreateExerciseForUser(userName, exercise);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(exercise);
            }
        }
        public async Task<ActionResult> Details(int id)
        {
            var name = User.Identity.Name;
            var userName = await _userService.GetLoggedUserNameEmail(name);
            var model = await _exerciseService.GetExercise(id, userName);
            if (model == null)
            {
                NotFound();
            }

            return View(model);
        }
        // GET: Exercise/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var name = User.Identity.Name;
            var userName = await _userService.GetLoggedUserNameEmail(name);
            var model = await _exerciseService.GetExercise(id, userName);
            if (model == null)
            {
                NotFound();
            }

            return View(model);
        }

        // POST: Exercise/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ExerciseViewModel exercise)
        {
            try
            {
                // TODO: Add update logic here
                var name = User.Identity.Name;
                var userName = await _userService.GetLoggedUserNameEmail(name);
                await _exerciseService.UpdateExercise(exercise, userName);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(exercise);
            }
        }

        // GET: Exercise/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var name = User.Identity.Name;
            var userName = await _userService.GetLoggedUserNameEmail(name);
            var model = await _exerciseService.GetExercise(id, userName);

            if (model == null)
            {
                NotFound();
            }

            return View(model);
        }

        // POST: Exercise/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(ExerciseViewModel exercise)
        {
            try
            {
                // TODO: Add delete logic here
                var name = User.Identity.Name;
                var userName = await _userService.GetLoggedUserNameEmail(name);
                await _exerciseService.DeleteExercise(exercise.Id, userName);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(exercise);
            }
        }
    }
}