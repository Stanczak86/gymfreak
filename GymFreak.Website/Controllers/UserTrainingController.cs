﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymFreak.Website.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GymFreak.Website.Controllers
{
    public class UserTrainingController : Controller
    {
        private readonly IUserTrainingService _userTrainingService;
        private readonly IUserService _userService;
        public UserTrainingController(IUserTrainingService userTrainingService, IUserService userService)
        {
            _userTrainingService = userTrainingService;
            _userService = userService;
        }

        // GET: UserTraining
        public ActionResult Index()
        {
            var userName = _userService.GetLoggedUserName();
            var model = _userTrainingService.GetAllUserTrainings(userName);

            return View(model);
        }

        // GET: UserTraining/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UserTraining/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserTraining/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserTraining/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: UserTraining/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserTraining/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: UserTraining/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}