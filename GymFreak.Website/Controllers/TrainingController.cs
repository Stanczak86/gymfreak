﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymFreak.Application.Interfaces;
using GymFreak.Infra.CrossCutting.Identity.Adapter;
using GymFreak.Website.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GymFreak.Website.Controllers
{
    [Authorize]
    public class TrainingController : Controller
    {
        private readonly ITrainingService _trainingService;
        private readonly IUserAdapter _userService;
        public TrainingController(ITrainingService trainingService, IUserAdapter userService)
        {
            _trainingService = trainingService;
            _userService = userService;
        }

        // GET: Training
        public async Task<ActionResult> Index()
        {
            var name = User.Identity.Name;
            var userName = await _userService.GetLoggedUserNameEmail(name);
            if (userName == null)
            {
                return NotFound();
            }

           // var model = _trainingService.GetAllTrainingsForUser(userName);

            return View();
        }

        // GET: Training/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Training/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Training/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Training/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Training/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Training/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Training/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}