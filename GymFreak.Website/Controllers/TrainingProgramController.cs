﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymFreak.Website.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GymFreak.Website.Controllers
{
    public class TrainingProgramController : Controller
    {

        private readonly ITrainingProgramService _trainingProgramService;
        private readonly IUserService _userService;
        public TrainingProgramController(ITrainingProgramService trainingProgramService, IUserService userService)
        {
            _trainingProgramService = trainingProgramService;
            _userService = userService;
        }

        // GET: TrainingProgram
        public ActionResult Index()
        {
            var userName = _userService.GetLoggedUserName();
            var model = _trainingProgramService.GetAllTrainingProgramsForUser(userName);

            return View(model);
        }

        // GET: TrainingProgram/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TrainingProgram/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TrainingProgram/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TrainingProgram/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TrainingProgram/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TrainingProgram/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TrainingProgram/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}