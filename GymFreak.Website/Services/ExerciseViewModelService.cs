﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using GymFreak.Application.DTO;
using GymFreak.Application.Interfaces;
using GymFreak.Website.Interfaces;
using GymFreak.Website.ViewModels.Exercise;
using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace GymFreak.Website.Services
{
    public class ExerciseViewModelService:IExerciseViewModelService
    {
        private readonly IExerciseService _exerciseService;
        private readonly IMapper _mapper;

        public ExerciseViewModelService(IExerciseService exerciseService, IMapper mapper)
        {
            _exerciseService = exerciseService;
            _mapper = mapper;
        }

        public async Task CreateExerciseForUser(string user, ExerciseViewModel exerciseVM)
        {
            var exerciseDto = _mapper.Map<ExerciseWithSpecificDto>(exerciseVM);
            await _exerciseService.AddExerciseAsync(user, exerciseDto); 
        }

        public async Task<List<ExerciseViewModel>> GetAllExercisesForUser(string user)
        {
            var list = await _exerciseService.GetAllExercisesForUserAsync(user);
            var mapList =  _mapper.Map<List<ExerciseViewModel>>(list);

            return mapList;
        }

        public async Task<ExerciseViewModel> GetExercise(int id, string userName)
        {
            var model = await _exerciseService.GetExerciseForUser(id,userName);
            var mapModel = _mapper.Map<ExerciseViewModel>(model);

            return mapModel;
        }

        public void RemoveExercise(int exerciseId)
        {
            throw new System.NotImplementedException();
        }

        public async Task UpdateExercise (ExerciseViewModel exerciseModel, string userName)
        {
            var model = await _exerciseService.GetExerciseForUser(exerciseModel.Id, userName);
            if (model != null)
            {
                var modelDto = _mapper.Map<ExerciseWithSpecificDto>(exerciseModel);
                modelDto.Id = model.Id;
                await _exerciseService.UpdateExerciseAsync(modelDto, userName);
            }
        }

        public async Task DeleteExercise(int id, string userName)
        {
            var model = await _exerciseService.GetExerciseForUser(id, userName);
            if (model != null)
            {
                await _exerciseService.DeleteExerciseAsync(id);
            }

        }
    }
}