﻿using System.Collections.Generic;
using GymFreak.Website.ViewModels.Exercise;

namespace GymFreak.Website.ViewModels.Training
{
    public class NewSchemaViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TrainingDay { get; set; }
        public List<ExerciseViewModel> ExerciseViewModels { get; set; }
        public ExerciseViewModel Exercise { get; set; }
    }
}
