﻿namespace GymFreak.Website.ViewModels.UserTraining
{
    public class UserTrainingSetViewModel
    {
        public int SetNumber { get; protected set; }
        public int SetReps { get; protected set; }
        public decimal Weight { get; protected set; }
    }
}