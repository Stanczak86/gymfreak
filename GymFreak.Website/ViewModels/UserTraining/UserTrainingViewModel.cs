﻿using System;

namespace GymFreak.Website.ViewModels.UserTraining
{
    public class UserTrainingViewModel
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }

    }
}