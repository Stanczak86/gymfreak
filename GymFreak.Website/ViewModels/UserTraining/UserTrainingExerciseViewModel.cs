﻿using System.Collections.Generic;
using System.Security.Permissions;
using GymFreak.Website.ViewModels.Exercise;

namespace GymFreak.Website.ViewModels.UserTraining
{
    public class UserTrainingExerciseViewModel: ExerciseViewModel
    {
        public List<UserTrainingSetViewModel> PersonalSetViewModels { get; set; }
    }
}