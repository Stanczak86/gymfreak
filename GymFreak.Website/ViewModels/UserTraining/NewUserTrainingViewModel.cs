﻿using System.Collections.Generic;

namespace GymFreak.Website.ViewModels.UserTraining
{
    public class NewUserTrainingViewModel: UserTrainingViewModel
    {
        public UserTrainingExerciseViewModel PersonalExerciseViewModel { get; set; }
        List<UserTrainingExerciseViewModel> ExerciseViewModels { get; set; }
    }
}