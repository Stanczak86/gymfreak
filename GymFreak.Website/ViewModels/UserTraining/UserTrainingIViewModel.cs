﻿using System;

namespace GymFreak.Website.ViewModels.UserTraining
{
    public class UserTraininglViewModel
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }

    }
}