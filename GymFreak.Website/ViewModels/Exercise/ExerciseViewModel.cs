﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GymFreak.Website.ViewModels.Exercise
{
    public class ExerciseViewModel
    {
        public int Id { get;  set; }
        [Display(Name = "Nazwa")]
        public string Name { get;  set; }
        [Display(Name = "Ilość powtórzeń")]
        public int Reps { get;  set; }
        [Display(Name = "Ilość serii")]
        public int Sets { get;  set; }
        [Display(Name = "Przerwa")]
        public TimeSpan ExerciseBreak { get;  set; }
        public string Measure { get;  set; }
    }
}