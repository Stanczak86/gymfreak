﻿using System.Collections.Generic;
using GymFreak.Website.ViewModels.Training;

namespace GymFreak.Website.ViewModels.TrainingProgram
{
    public class NewProgramViewModel:TrainingProgramViewModel
    {
        public List<SchemaViewModel> Trainings { get; protected set; }
    }
}