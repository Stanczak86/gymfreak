﻿namespace GymFreak.Website.ViewModels.TrainingProgram
{
    public class TrainingProgramViewModel
    {
        public string Name { get; protected set; }
        public bool IsActual { get; protected set; }
    }
}