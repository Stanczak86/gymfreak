﻿using AutoMapper;
using GymFreak.Application.DTO;
using GymFreak.Website.ViewModels.Exercise;

namespace GymFreak.Website.AutoMapper
{
    public class ViewModelToDTOMappingProfile:Profile
    {
        public ViewModelToDTOMappingProfile()
        {
            CreateMap<ExerciseViewModel, ExerciseDto> ();
            CreateMap<ExerciseViewModel, ExerciseWithSpecificDto>();
        }
    }
}