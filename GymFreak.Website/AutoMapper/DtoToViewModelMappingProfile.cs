﻿using AutoMapper;
using GymFreak.Application.DTO;
using GymFreak.Website.ViewModels.Exercise;

namespace GymFreak.Website.AutoMapper
{
    public class DtoToViewModelMappingProfile:Profile
    {
        public DtoToViewModelMappingProfile()
        {
            CreateMap<ExerciseDto, ExerciseViewModel>()
                .ForMember(x => x.Name, m => m.MapFrom(p => p.Name));
        }
        
    }
}