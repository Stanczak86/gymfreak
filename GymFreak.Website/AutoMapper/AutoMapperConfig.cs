﻿using AutoMapper;

namespace GymFreak.Website.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static IMapper RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DtoToViewModelMappingProfile());
                cfg.AddProfile(new ViewModelToDTOMappingProfile());
            }).CreateMapper();
        }
    }
}